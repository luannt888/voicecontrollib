package vn.mediatech.voicecontrol.log

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import vn.mediatech.voicecontrol.R
import vn.mediatech.voicecontrol.TypeWriterTextView
import java.util.*

class ItemLogAdapter(
    val context: Context,
    val itemList: ArrayList<ItemLog>
) : RecyclerView.Adapter<ViewHolder>() {
    var onItemClickListener: OnItemClickListener? = null
    var isTyping:Boolean = true

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.item_log_vc,
            parent, false
        )
        return FrameViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemObj: ItemLog = itemList[position]
        if (holder is FrameViewHolder) {
            bindDataFrameView(holder, itemObj, position)
        }
    }

    fun bindDataFrameView(holder: FrameViewHolder, itemObj: ItemLog, position: Int) {
        if (itemObj.type == 1) {
            holder.layoutRight.visibility = View.GONE
            holder.layoutLeft.visibility = View.VISIBLE
            if (position == itemCount - 1 && isTyping) {
                holder.textMsgLeft.text = ""
                holder.textMsgLeft.setCharacterDelay(45);
                holder.textMsgLeft.animateText(itemObj.message)
            } else {
                holder.textMsgLeft.text = itemObj.message
            }
        } else {
            holder.layoutLeft.visibility = View.GONE
            holder.layoutRight.visibility = View.VISIBLE
            if (position == itemCount - 1&& isTyping) {
                holder.textMsgRight.text = ""
                holder.textMsgRight.setCharacterDelay(10);
                holder.textMsgRight.animateText(itemObj.message)
            } else {
                holder.textMsgRight.text = itemObj.message
            }
        }
        if(position == itemCount - 1){
            isTyping = true
        }
        holder.layoutRoot.setOnClickListener {
            onItemClickListener?.onClick(itemObj, position)
        }
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    inner class FrameViewHolder(itemView: View) : ViewHolder(itemView) {
        val textMsgLeft: TypeWriterTextView = itemView.findViewById(R.id.textMsgLeft)
        val textMsgRight: TypeWriterTextView = itemView.findViewById(R.id.textMsgRight)
        val layoutRoot: RelativeLayout = itemView.findViewById(R.id.layoutRoot)
        val layoutRight: LinearLayout = itemView.findViewById(R.id.layoutRight)
        val layoutLeft: LinearLayout = itemView.findViewById(R.id.layoutLeft)
    }

    interface OnItemClickListener {
        fun onClick(itemObject: ItemLog, position: Int)
    }
}